import { Component } from "react";

class InputContent extends Component {
    inputChangeHandler = (event) => {
        const value = event.target.value;

        this.props.inputMessageChangeHandlerProp(value);
    }

    buttonClickHandler = () => {
        this.props.outputMessageChangeHandlerProp();
    }
    


    render() {
        const {inputMessageProp} = this.props;
        return (
            <>
                <div className='row mt-3'>
                    <div className='col-12'>
                    <label>Message cho bạn 12 tháng tới</label>
                    </div>
                </div>  
                <div className='row mt-3'>
                    <div className='col-12'>
                    <input id="inputMessage" value={inputMessageProp} onChange={this.inputChangeHandler} placeholder="Hãy nhập thông điệp" className='form-control' />
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                    <button onClick={this.buttonClickHandler} className='btn btn-primary'>Gửi thông điệp</button>
                    </div>
                </div>              
            </>
        )
    }
}

export default InputContent
