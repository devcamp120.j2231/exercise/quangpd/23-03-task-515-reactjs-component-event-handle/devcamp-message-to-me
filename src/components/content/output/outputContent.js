import { Component } from "react";
import like from "../../../assets/images/like.png"

class OutputContent extends Component {
    
    render() {
        const {outputMessageProp,likeDisplayProp} = this.props;
        return (
            <>
                <div className='row mt-3'>
                    <div className='col-12'>
                        {outputMessageProp.map((element, index) => {
                            return <p key={index}>{element}</p>
                        })}   
                    </div>
                </div>  
                <div className='row'>
                    <div className='col-12'>
                        {likeDisplayProp ? <img src={like} width = "100px" alt="like"/> : null}
                    </div>
                </div>              
            </>
        )
    }
}

export default OutputContent
