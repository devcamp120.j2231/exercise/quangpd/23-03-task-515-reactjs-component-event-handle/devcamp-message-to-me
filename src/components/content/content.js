import { Component } from "react";
import InputContent from "./input/inputContent";
import OutputContent from "./output/outputContent";

class Content extends Component {

    constructor(props){
        super(props)

        this.state = {
            inputMessage : "",
            outputMessage : [],
            likeDisplay : false
        }
    }

    inputMessageChangeHandler =(value) =>{
        this.setState({
            inputMessage : value
        })
    }

    outputMessageChangeHandler = () =>{
        this.setState({
            outputMessage :[...this.state.outputMessage, this.state.inputMessage],
            likeDisplay : true
        })
    }
    render() {
        return (
            <>
                <InputContent 
                    inputMessageProp={this.state.inputMessage}
                    inputMessageChangeHandlerProp = {this.inputMessageChangeHandler}
                    outputMessageChangeHandlerProp = {this.outputMessageChangeHandler}

                />
                <OutputContent 
                    outputMessageProp ={this.state.outputMessage}
                    likeDisplayProp = {this.state.likeDisplay}
                />
            </>
        )
    }
}

export default Content
