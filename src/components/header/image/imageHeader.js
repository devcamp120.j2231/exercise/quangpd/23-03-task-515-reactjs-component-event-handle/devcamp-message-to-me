import { Component } from "react";
import banner from '../../../assets/images/employee.jpg'

class ImageHeader extends Component {
    render() {
        return(
            <>
                <div className='row'>
                    <div className='col-12'>
                    <img src={banner} width="600px" alt="background"/>
                    </div>
                </div>  
            </>
        )
    }
}

export default ImageHeader
