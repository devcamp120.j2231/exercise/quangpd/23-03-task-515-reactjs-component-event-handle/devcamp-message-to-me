import 'bootstrap/dist/css/bootstrap.min.css'


import Content from './components/content/content';
import Header from './components/header/header';
function App() {
  return (
    <div className='container text-center mt-5'>
      <Header/>
      <Content/>
    </div>
  );
}

export default App;
